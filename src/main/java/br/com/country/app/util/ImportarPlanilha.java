package br.com.country.app.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.country.app.model.Cidade;

public class ImportarPlanilha {

	public List<Cidade> csvParaList(BufferedReader reader) {

        try {
        	
            String linha = null;
            List<Cidade> cidades = new ArrayList<>();
            int i = 0;
            
            while ((linha = reader.readLine()) != null) {
            	
                if (i > 0) {
                	
                    String[] dados = linha.split(",");
                    cidades.add(novoCidade(dados));
                    
                } else {
                	
                    i++;
                }
            }

            reader.close();

            return cidades;

        } catch (IOException e) {
        	
            e.printStackTrace();
            
            return null;
        }
    }

    private Cidade novoCidade(String[] dados) {

        Cidade cidade = new Cidade();
        cidade.setIbge_id(Long.parseLong(dados[0]));
        cidade.setUf(dados[1]);
        cidade.setName(dados[2]);
        cidade.setCapital(Util.stringToBoolean(dados[3]));
        cidade.setLon(Double.parseDouble(dados[4]));
        cidade.setLat(Double.parseDouble(dados[5]));
        cidade.setNo_accents(dados[6]);
        cidade.setAlternative_names(dados[7]);
        cidade.setMicroregion(dados[8]);
        cidade.setMesoregion(dados[9].trim());

        return cidade;
    }
}
