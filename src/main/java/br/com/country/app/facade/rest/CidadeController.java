package br.com.country.app.facade.rest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.input.BOMInputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.country.app.domain.LatitudeLongitude;
import br.com.country.app.facade.rest.exception.CitiesApplicationException;
import br.com.country.app.model.Cidade;
import br.com.country.app.model.Estado;
import br.com.country.app.service.CidadeService;
import br.com.country.app.util.ImportarPlanilha;

@RestController
@RequestMapping("/city")
public class CidadeController {

    @Autowired
    private CidadeService cidadeService;
    
    /**
     * Ler o arquivo CSV das cidades para a base de dados
     * 
     * @param uploadfile
     * @return
     * @throws CitiesApplicationException
     */
	@PostMapping(path= "/import",consumes=MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> importCsv(@RequestParam("uploadfile") MultipartFile uploadfile) throws CitiesApplicationException {
		
		try {
			
			ImportarPlanilha csv = new ImportarPlanilha();
			BufferedReader reader = new BufferedReader(new InputStreamReader(new BOMInputStream(uploadfile.getInputStream()), "UTF-8"));
			List<Cidade> listCidades = csv.csvParaList(reader);
			
	        if (null == listCidades) {
	        	
	        	return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	            
	        } else {
	        	
	            cidadeService.inserir(listCidades);
	            return new ResponseEntity<>(HttpStatus.CREATED);
	        }
			
		} catch (Exception e) {
			
			return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * Permitir adicionar uma nova cidade
	 * 
	 * @param city
	 * @return
	 */
	@RequestMapping(value = "/addcity", method = RequestMethod.POST)
    public ResponseEntity<?> addCity(@RequestBody Cidade city){
    	
    	try {
    		
    		return new ResponseEntity<>(HttpStatus.CREATED);
    		
    	} catch (Exception e) {
			
	    	return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
		}
    }
	
	/**
	 * Retornar somenta as cidades que são capitais
	 * 
	 * @return
	 */
    @GetMapping("/capitals")
    public ResponseEntity<?> capitalCities() {
    	
    	try {
    		
	        List<Cidade> municipio = cidadeService.buscarCapitais();
	        return  new ResponseEntity<> (municipio, HttpStatus.OK);
	        
    	} catch (Exception e) {
			
    		return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
		}
    }

    /**
     * Obter os dados da cidade informando o id do IBGE
     */
    @GetMapping("/citybyibge/{id}")
    public ResponseEntity<?> citybyIbge(@PathVariable("id") long id) {
    	
    	try {
    	
    		Cidade city = cidadeService.buscarCidadePorID(id);
    		return new ResponseEntity<> (city, HttpStatus.OK);
    		
    	} catch (Exception e) {
			
    		return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
		}
    }

    /**
     * Lista simples de cidades
     * 
     * @return
     */
    @GetMapping
    public ResponseEntity<?> listarCidades() {
    	
    	try {
    		
    		List<Cidade> cities = cidadeService.listarCidades();
    		return new ResponseEntity<> (cities, HttpStatus.OK);
        	
        
    	} catch (Exception e) {
			
    		return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
		}
    }
    
    /**
     * Retornar a quantidade de registros totais
     * 
     * @return
     */
    @GetMapping("/citytotal")
    public ResponseEntity<?> cityTotal() {
    	
        try {
        	
    		long sum = cidadeService.totalRegistros();
    		return  new ResponseEntity<> (sum, HttpStatus.OK);
    		
    	} catch (Exception e) {
			
    		return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
		}
    }

    /**
     * Retornar o nome das cidades baseado em um estado selecionado
     * 
     * @param uf
     * @return
     */
    @GetMapping("/namecitybystate/{uf}")
    public ResponseEntity<?> nameCityByState(@PathVariable("uf") String uf) {
    	
    	try {
    		
	    	List<Cidade> cities = cidadeService.buscarCidadePorUF(uf.toUpperCase());
	    	return  new ResponseEntity<> (cities, HttpStatus.OK);
		
		} catch (Exception e) {
			
			return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
		}
    }

    /**
     * Retornar a quantidade de registro baseado em uma coluna. Não deve contar itens iguais

     * @param coluna
     * @return
     */
    @GetMapping("/countbycolumn/{coluna}")
    public ResponseEntity<?> countByColumn(@PathVariable("coluna") String coluna) {
    	
    	try {
    		
    		long count = cidadeService.totalRegistrosPorColuna(coluna);
	    	return  new ResponseEntity<> (count, HttpStatus.OK);
		
		} catch (Exception e) {
			
			return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
		}
    }

    /**
     * Permitir selecionar uma coluna do CSV e através dela entrar com uma string
	 * para filtrar. Retornar assim todos os objetos com tal string

     * @param coluna
     * @param valor
     * @return
     */
    @GetMapping("/searchbycolumn/{coluna}/{valor}")
    public ResponseEntity<?> searchByColumn(@PathVariable("coluna") String coluna, @PathVariable("valor") String valor) {
    	
		try {
		    		
			List<Cidade> cities = cidadeService.buscarPorColunaEValor(coluna, valor);
	    	return  new ResponseEntity<> (cities, HttpStatus.OK);
		
		} catch (Exception e) {
			
			return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
		}
    }

    /**
     * Retornar a quantidade de cidades por estado
     * 
     * @return
     */
    @GetMapping("/number-of-cities-by-state")
    public ResponseEntity<?> numberOfCitiesByState() {
    	
    	try {
    		
    		List<Estado> ufs = cidadeService.totalCidadesPorEstado();
    		return new ResponseEntity<> (ufs, HttpStatus.OK);
	
		} catch (Exception e) {
			
			return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
		}
    }

    /**
     * Retornar o nome do estado com o maior e menor quantidade de cidades e
	 * a quantidade de cidades

     * @return
     */
    @GetMapping("/statemoreandlesscity")
   public ResponseEntity<?> stateWithMoreAndLessAmountOfCity() {
    	
    	try {
    	
	        List<Estado> estados = new ArrayList<Estado>();
	        estados.add(cidadeService.minCidadesPorEstado().get(0));
	        estados.add(cidadeService.maxCidadesPorEstado().get(0));
        
	    	return new ResponseEntity<> (estados, HttpStatus.OK);
	    	
		} catch (Exception e) {
			
			return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
		}
    }

    /**
     * Permitir deletar uma cidade
     * 
     * @param id
     * @return
     * @throws CitiesApplicationException
     */
    @DeleteMapping(path= "/{id}")
    public ResponseEntity<?> remove(@PathVariable("id") long id) throws CitiesApplicationException {
    	
    	try {
    		
	        cidadeService.deletarPorId(id);
	        return new ResponseEntity<>(HttpStatus.OK);
        
	    } catch (Exception e) {
			
	    	return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
		}
    }

    /**
     * Dentre todas as cidades, obter as duas cidades mais distantes uma da outra
     * com base na localização (distância em KM em linha reta)

     * @return
     */
    @GetMapping("/further")
    public ResponseEntity<?> further() {
    	
    	try {
    	
	        LatitudeLongitude calc = new LatitudeLongitude();
	        Object[] array = calc.maiorDistancia(cidadeService.listarCidades()).toArray();	        
	        return new ResponseEntity<> (array, HttpStatus.OK);
	    	
		} catch (Exception e) {
			
			return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
		}
    }

}
