/**
 * 
 */
package br.com.country.app.facade.rest.exception;

public class CitiesApplicationException extends Exception {

	private static final long serialVersionUID = 1L;

	public CitiesApplicationException() {}

    public CitiesApplicationException(String message) {
        super(message);
    }

    public CitiesApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public CitiesApplicationException(Throwable cause) {
        super(cause);
    }
}
