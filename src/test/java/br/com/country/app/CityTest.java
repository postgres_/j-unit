package br.com.country.app;

import static br.com.country.app.domain.FakeDomain.city;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Locale;

import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import br.com.country.app.domain.NotFoundException;
import br.com.country.app.domain.WebTestConfig;
import br.com.country.app.facade.rest.CidadeController;
import br.com.country.app.model.Cidade;
import br.com.country.app.service.CidadeService;


@RunWith(Arquillian.class)
@RunAsClient
public class CityTest {

	private MockMvc mockMvc;

    @Mock
    private CidadeService cityService;

    @InjectMocks
    private CidadeController controller;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setHandlerExceptionResolvers(WebTestConfig.restErrorHandler())
                .setLocaleResolver(WebTestConfig.fixedLocaleResolver(Locale.getDefault()))
                .setMessageConverters(WebTestConfig.jacksonDateTimeConverter())
                .setValidator(WebTestConfig.validator())
                .setCustomArgumentResolvers(WebTestConfig.pageRequestArgumentResolver()).build();
    }
	
    /**
     * Obter os dados da cidade informando o id do IBGE
     * 
     * @throws Exception
     */
    @Test
    public void testFindByIdWithSuccess() throws Exception {
        Cidade city = city();
        given(cityService.buscarCidadePorID(1)).willReturn(city);

        mockMvc.perform(get("/1")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.ibge_id").value(city.getIbge_id()))
                .andExpect(jsonPath("$.uf").value(city.getUf()))
                .andExpect(jsonPath("$.name").value(city.getName()))
                .andExpect(jsonPath("$.capital").value(city.isCapital()))
                .andExpect(jsonPath("$.lon").value(city.getLon()))
                .andExpect(jsonPath("$.lat").value(city.getLat()))
                .andExpect(jsonPath("$.no_accents").value(city.getNo_accents()))
                .andExpect(jsonPath("$.alternative_name").value(city.getAlternative_names()))
                .andExpect(jsonPath("$.microregion").value(city.getMicroregion()))
                .andExpect(jsonPath("$.mesoregion").value(city.getMesoregion()));
    }
    
    /**
     * Obter os dados da cidade informando o id do IBGE
     * 
     * @throws Exception
     */
    @Test
    public void testFindByIdWithException() throws Exception {
        given(cityService.buscarCidadePorID(1)).willThrow(NotFoundException.class);

        mockMvc.perform(get("/1")).andExpect(status().isNotFound());
    }
}
