package br.com.country.app.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApiErrorResponse {

	private HttpStatus status = HttpStatus.BAD_REQUEST;
	private String message;
	private List<String> errors;
	
	public ApiErrorResponse() {}
	
	public ApiErrorResponse(String message) {
		this.message = message;
		errors = new ArrayList<>();
	}
	
	public ApiErrorResponse(HttpStatus status, String message, List<String> errors) {
		this.message = message;
		this.errors = errors;
		this.status = status;
	}
	
	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public void addError(String error) {
	    errors.add(error);
	}
}
