package br.com.country.app.domain;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.country.app.model.Cidade;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CityForm {

    @JsonProperty("ibge_id")
    @Min(1)
    private long id;

    @JsonProperty("uf")
    @NotBlank
    private String uf;

    @JsonProperty("name")
    @NotBlank
    private String name;

    @JsonProperty("capital")
    private boolean capital;

    @JsonProperty("lon")
    @NotNull
    private Double lon;

    @JsonProperty("lat")
    @NotNull
    private Double lat;

    @JsonProperty("no_accents")
    @NotBlank
    private String noAccents;

    @JsonProperty("alternative_name")
    private String alternativeNames;

    @JsonProperty("microregion")
    @NotBlank
    private String microregion;

    @JsonProperty("mesoregion")
    @NotBlank
    private String mesoregion;
    
    public Cidade toCity() {
    	Cidade city = new Cidade();
        city.setIbge_id(id);
        city.setUf(uf);
        city.setName(name);
        city.setCapital(capital);
        city.setLon(lon);
        city.setLat(lat);
        city.setNo_accents(noAccents);
        city.setAlternative_names(alternativeNames);
        city.setMicroregion(microregion);
        city.setMesoregion(mesoregion);
        return city;
    }

    public CityForm(long id,
         String uf,
         String name,
         boolean capital,
         Double lon,
         Double lat,
         String noAccents,
         String alternativeNames,
         String microregion,
         String mesoregion) {
    	
    	this.id = id;
    	this.uf = uf;
    	this.name = name;
    	this.capital = capital;
    	this.lon = lon;
    	this.lat = lat;
    	this.noAccents = noAccents;
    	this.alternativeNames = alternativeNames;
    	this.microregion = microregion;
    	this.mesoregion = mesoregion;
    	
    }
    public static CityForm of(Cidade city) {
        return new CityForm(city.getIbge_id(), city.getUf(), city.getName(), city.isCapital(),
                city.getLon(), city.getLat(), city.getNo_accents(), city.getAlternative_names(),
                city.getMicroregion(), city.getMesoregion());
    }
}
