package br.com.country.app.domain;

import java.util.Locale;

import com.github.javafaker.Faker;

import br.com.country.app.model.Cidade;

public final class FakeDomain {
	
    private static final Faker faker = new Faker(new Locale("pt-BR"));
    
    public static Cidade city() {
    	Cidade city = new Cidade();
        city.setIbge_id(faker.number().numberBetween(1, 100000));
        city.setUf(faker.address().stateAbbr());
        city.setName(faker.address().cityName());
        city.setAlternative_names("");
        city.setCapital(faker.number().numberBetween(0, 1) == 1);
        city.setLat(Double.parseDouble(faker.address().latitude().replaceAll(",", ".")));
        city.setLon(Double.parseDouble(faker.address().longitude().replaceAll(",", ".")));
        city.setMesoregion(faker.address().cityName());
        city.setMicroregion(faker.address().cityName());
        city.setNo_accents(city.getName());
        return city;
    }
}
