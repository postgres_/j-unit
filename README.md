## Pilha de tecnologias
```
	* Spring Boot
	* Postgres
	* Java 8.1
	* Maven
```

## Executando a aplicação

1. Para rodar a aplicação no eclipse [botão direito: Run As/Java Application] em cima do arquivo
 "/CityApp/src/main/java/br/com/country/app/AppApplication.java".

2. Passe as informações da base de dados Postgres no arquivo 
"/CityApp/src/main/resources/application.properties", conforme exemplo:

```
	spring.datasource.url=jdbc:postgresql://localhost:5432/CountryAppDB
	spring.datasource.username=postgres
	spring.datasource.password=postgres
```
	
3. A tabela é criada automaticamente, mas caso precise:

```
    CREATE TABLE public.cidade
	(
	    ibge_id bigint NOT NULL,
	    alternative_names character varying(255) COLLATE pg_catalog."default",
	    capital boolean NOT NULL,
	    lat double precision,
	    lon double precision,
	    mesoregion character varying(255) COLLATE pg_catalog."default",
	    microregion character varying(255) COLLATE pg_catalog."default",
	    name character varying(255) COLLATE pg_catalog."default",
	    no_accents character varying(255) COLLATE pg_catalog."default",
	    uf character varying(255) COLLATE pg_catalog."default",
	    CONSTRAINT cidade_pkey PRIMARY KEY (ibge_id)
	)
```

## API's disponíveis

# 1. Ler o arquivo CSV das cidades para a base de dados;
```
HTTP : POST	-	Path : http://localhost:8080/city/import

	Body
	"form-data"
	"Key" : "uploadfile"	-	"File"		-	"Value" : "Trabalho Java - Cidades.csv"	
```

# 2. Retornar somente as cidades que são capitais ordenadas por nome;
```
HTTP : GET	-	Path : http://localhost:8080/city/capitals
```
# 3. Retornar o nome do estado com a maior e menor quantidade de cidades e a quantidade de cidades;
```
HTTP : GET	-	Path : http://localhost:8080/city/statemoreandlesscity
```
# 4. Retornar a quantidade de cidades por estado;
```
HTTP : GET	-	Path : http://localhost:8080/city/number-of-cities-by-state
```
# 5. Obter os dados da cidade informando o id do IBGE;
```
(Exemplo: 4202404 - Blumenau SC)

HTTP : GET	-	Path : http://localhost:8080/city/citybyibge/4202404
```
# 6. Retornar o nome das cidades baseado em um estado selecionado;
```
(Exemplo: RS - Rio Grande do Sul)

HTTP : GET	-	Path : http://localhost:8080/city/namecitybystate/rs
```
# 7. Permitir adicionar uma nova Cidade;
```
HTTP : POST		-	Path : http://localhost:8080/city/addcity

Body : 
	{
	    "ibge_id": 103,
	    "uf": "EX",
	    "name": "Example_name",
	    "capital": false,
	    "lon": -72.9165010261,
	    "lat": -7.5932225939,
	    "no_accents": "Example_no_accents",
	    "alternative_names": "Example_alternative_names",
	    "microregion": "Example_microregion",
	    "mesoregion": "Example_mesoregion"
	}
```
# 8. Permitir deletar uma cidade;
```
(Exemplo: Cidade de teste criada anteriormente - 103)

HTTP: DELETE	-	Path : http://localhost:8080/city/103
```
# 9. Permitir selecionar uma coluna (do CSV) e através dela entrar com uma string para filtrar. retornar assim todos os objetos que contenham tal string;
```
(Exemplo: Busca por nome - Panambi)

HTTP: GET	-	Path : http://localhost:8080/city/searchbycolumn/name/panambi

A lista de colunas disponíveis são:

* ibge_id (número inteiro)
* uf (string)
* name (string)
* capital (true ou false)
* no_accents (string)
* alternative_names (string)
* microregion (string)
* mesoregion (string)
* lat (número decimal)
* lon (número decimal)
```
# 10. Retornar a quantidade de registro baseado em uma coluna. Não deve contar itens iguais;
```
(Exemplo: Busca quantidade de estados)

HTTP : GET	-	Path : http://localhost:8080/city/countbycolumn/uf
```
# 11. Retornar a quantidade de registros total;
```
HTTP : GET	-	Path : http://localhost:8080/city/citytotal
```
# 12. Dentre todas as cidades, obter as duas cidades mais distantes uma da outra com base na localização (distância em KM em linha reta);
```
HTTP : GET	-	Path : http://localhost:8080/city/further
```